#include <boost/asio.hpp>
#include "ServerClientFactory.h"

using boost::asio::ip::tcp;

class ChatHandler;
class TcpConnection;

class TcpServer: public IServer, public std::enable_shared_from_this<TcpServer>
{
public:
    TcpServer(boost::asio::io_service& io_service, ChatHandler& chathandler );
    virtual void Start() override;

private:
    void HandleAccept(std::shared_ptr<TcpConnection> connection,
                        const boost::system::error_code& error);


    void StartTimer();
    void HandleTimer(const boost::system::error_code& error);
    void BroadcastMessages();

    boost::asio::io_service& m_ioService;
    tcp::acceptor m_acceptor;
    boost::asio::deadline_timer m_broadcastTimer;
    ChatHandler& m_chatHandler;

    std::vector<std::shared_ptr<TcpConnection>> m_connections;
};
