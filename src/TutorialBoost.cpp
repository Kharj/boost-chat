#include<iostream>
#include<thread>
#include <condition_variable>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include<boost/lexical_cast.hpp>
#include "TcpServer.h"
#include "TcpClient.h"
#include "UdpClient.h"
#include "UdpServer.h"
#include "ChatHandler.h"
#include "ServerClientFactory.h"

using boost::asio::ip::tcp;

void WorkerThread(boost::asio::io_service& ioService)
{
    try
    {
        ioService.run();
    }
    catch (std::exception & e)
    {
        std::cerr << e.what() << std::endl;
    }
}

void ChatServerThread(ChatHandler& chatHandler, bool& chatThreadStopFlag)
{
    while (!chatThreadStopFlag)
    {
        {
            std::lock_guard<std::mutex> guard(chatHandler.m_buffersMutex);
            while (!chatHandler.m_incomingBuffer.empty())
            {
                ChatMessage msg = std::move(chatHandler.m_incomingBuffer.front());
                chatHandler.m_incomingBuffer.pop_front();
                if( msg.m_username != chatHandler.m_username )
                {
                    std::cout << "[" + msg.m_username + "] " + msg.m_text << std::endl;
                }
                //chatHandler.m_outcomingBuffer.emplace_back(std::move(msg));
                chatHandler.m_outcomingBuffer.push_back(msg);
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
    }
}

void ChatClientThread(ChatHandler& chatHandler, bool& chatThreadStopFlag)
{
}

int main( int argc, char * argv[] )
{
    boost::asio::io_service ioService;
    boost::asio::io_service::work work(ioService);
    std::thread asioThread(&WorkerThread, std::ref(ioService));
    //std::thread asioThread2(&WorkerThread, std::ref(ioService));

    ChatHandler chatHandler;
    chatHandler.m_port = 7777;
    chatHandler.m_serverIp = "127.0.0.1";
    chatHandler.m_username = "server";

    std::shared_ptr<IServer> server;
    std::shared_ptr<IClient> client;

    ServerClientFactory::Protocol protocol;
    int protocolInt = 0;
    int command = 0;
    if (argc > 1)
    {
        command = std::stoi( argv[1] );
    }
    while( command != 1 && command != 2 )
    {
        std::cout << "Enter 1 to start Server, 2 to start Client: ";
        std::cin >> command;
    }

    if (argc > 2)
    {
        if (!strcmp(argv[2], "tcp"))
        {
            protocol = ServerClientFactory::Protocol::k_TCP;
        }
        else if (!strcmp(argv[2], "udp"))
        {
            protocol = ServerClientFactory::Protocol::k_UDP;
        }
        protocolInt = 1;
    }

    while (protocolInt != 1 && protocolInt != 2)
    {
        std::cout << "Enter 1 for TCP, 2 for UDP: ";
        std::cin >> protocolInt;
        protocol = (ServerClientFactory::Protocol)(protocolInt - 1);
    }

    if (argc > 3)
    {
        chatHandler.m_serverIp = std::string(argv[3]);
    }
    else
    {
        if (command == 2)
        {
            std::cout << "Enter server ip, or press enter to use localhost: ";
            std::cin >> chatHandler.m_serverIp;
            if (chatHandler.m_serverIp.empty())
            {
                chatHandler.m_serverIp = "127.0.0.1";
            }
        }
    }


    if( command == 1)
    {
        server = std::shared_ptr<IServer>( ServerClientFactory::CreateServer(protocol, ioService, chatHandler));
        server->Start();

        //Run chat thread
        bool chatThreadStopFlag = false;
        std::thread chatThread(&ChatServerThread, std::ref(chatHandler), std::ref(chatThreadStopFlag));
        std::cout << "Press any key to stop server.\n";
        std::cin.ignore();
        std::cin.get();

        chatThreadStopFlag = true;
        chatThread.join();
    }
    else if( command == 2 )
    {
        std::cout << "Enter your name: ";
        std::cin >> chatHandler.m_username;

        client = std::shared_ptr<IClient>(ServerClientFactory::CreateClient(protocol, ioService, chatHandler));
        client->Start();
        std::cout << "Waiting for connection...\n";

        std::mutex waitConnectMutex;
        std::unique_lock<std::mutex> waitConnectLock(waitConnectMutex);
        client->GetConnectedEvent().wait(waitConnectLock,
            [&client]() {return client->IsConnected(); });

        //Run chat thread
        bool chatThreadStopFlag = false;
        std::thread chatThread(&ChatServerThread, std::ref(chatHandler), std::ref(chatThreadStopFlag));

        while (true)
        {
            //std::cout << "Enter message or enter /exit to stop client:\n";
            std::string msg;
            std::cin >> msg;
            if (msg == "/exit" || !client->IsConnected())
            {
                break;
            }
            else
            {
                //std::lock_guard<std::mutex> lg(chatHandler.m_buffersMutex);
                //chatHandler.m_outcomingBuffer.emplace_back( ChatMessage( msg ) );
                client->SendText(msg);
            }
        }
        chatThreadStopFlag = true;
        chatThread.join();
    }


    //Wait
    ioService.stop();
    asioThread.join();
    //asioThread2.join();

    return 0;
}
