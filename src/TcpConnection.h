#pragma once

#include <deque>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
using boost::asio::ip::tcp;

class ChatHandler;
class ChatMessage;

class TcpConnection : public std::enable_shared_from_this<TcpConnection>
{
public:
    TcpConnection(boost::asio::io_service& io_service, ChatHandler& chathandler);
    tcp::socket& GetSocket();
    void StartRead();
    bool IsClosed() const;

    void SendMessage(const ChatMessage& msg);
    void SendMessageQueue(const std::deque<ChatMessage>& msgs);

    static const std::string    k_delimeter;
private:

    void StartRead(std::shared_ptr<boost::asio::streambuf> streamBuffer);
    void HandleRead(std::shared_ptr<boost::asio::streambuf> streamBuffer,
        const boost::system::error_code& error, const size_t bytesTransferred);

    void StartWrite( const std::string& txt );

    boost::asio::io_service&    m_ioService;
    tcp::socket                 m_socket;
    ChatHandler&                m_chatHandler;
    bool                        m_isClosed{ false };
};
