#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "UdpServer.h"
#include "ChatHandler.h"

UdpServer::UdpServer(boost::asio::io_service& io_service, ChatHandler& chatHandler):
    m_ioService(io_service),
    m_broadcastTimer(io_service),
    m_chatHandler(chatHandler),
    m_socket(io_service, udp::endpoint(udp::v4(), 7777))
{
}

void UdpServer::Start()
{
    StartReceive();
}

void UdpServer::StartReceive()
{
    /*m_socket.async_receive_from(
        boost::asio::buffer(m_recv_buffer), m_remote_endpoint,
        boost::bind(&UdpServer::HandleReceive, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));*/


    auto self = shared_from_this();
    auto bufString = std::make_shared<std::string>();
    auto bufArray = std::make_shared<boost::array<char, 128>>();
    //auto incomingBuffer = boost::asio::buffer(*bufArray);
    auto remoteEndpoint = std::make_shared<udp::endpoint>();

    m_socket.async_receive_from(boost::asio::buffer(*bufArray), *remoteEndpoint,
        [self, remoteEndpoint, bufString, bufArray](const boost::system::error_code& error, size_t bytesTransferred)
		{
			if (!error)
			{
				std::cout << "[UdpServer]Received " << bytesTransferred << "bytes\n";
				{
                    std::string res(bufArray->data(), bufArray->size());
					std::lock_guard<std::mutex> guard(self->m_chatHandler.m_buffersMutex);
					self->m_chatHandler.m_incomingBuffer.emplace_back(ChatMessage::Deserialize(res));
				}
                self->StartReceive();
			}
			else if (error == boost::asio::error::connection_reset ||
				error == boost::asio::error::eof)
			{
				std::cout << "Client disconnected." << " \n";
			}
			else
			{
				std::cout << "Error reading" << error << " \n";
			}
		});



}

void UdpServer::HandleReceive(const boost::system::error_code& error, size_t bytesTransferred)
{
    if (!error)
    {
        std::cout << "[UdpServer]Received\n";
        //Send response
        boost::shared_ptr<std::string> message(
            new std::string());

        /*m_socket.async_send_to(boost::asio::buffer(*message), m_remote_endpoint,
                               boost::bind(&UdpServer::HandleSend, this, message,
                                           boost::asio::placeholders::error,
                                           boost::asio::placeholders::bytes_transferred));
                                           */
    }
    StartReceive();//restarting
}

void UdpServer::HandleSend(boost::shared_ptr<std::string> message,
                           const boost::system::error_code& error,
                           std::size_t bytes_transferred)
{
    std::cout<<"[UdpServer]Sent "<<bytes_transferred<<"bytes\n";
}
