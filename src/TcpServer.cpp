#include <iostream>
#include <cassert>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "TcpServer.h"
#include "TcpConnection.h"
#include "ChatHandler.h"

TcpServer::TcpServer(boost::asio::io_service& io_service, ChatHandler& chatHandler):
	m_ioService(io_service),
	m_acceptor(io_service, tcp::endpoint(tcp::v4(), 7777)),
	m_broadcastTimer(io_service),
	m_chatHandler(chatHandler)
{
}

void TcpServer::Start()
{
	auto newConnection = std::make_shared<TcpConnection>(m_ioService, m_chatHandler);

	m_acceptor.async_accept(newConnection->GetSocket(),
		boost::bind(&TcpServer::HandleAccept, shared_from_this(),
			newConnection,
			boost::asio::placeholders::error));

	StartTimer();
}

void TcpServer::HandleAccept(std::shared_ptr<TcpConnection> connection,
	const boost::system::error_code& error)
{
	if (!error)
	{
		connection->StartRead();
		m_connections.push_back(connection);

		std::cout << "Accepted new peer\n";
	}
	else
	{
		std::cout << "Error accepting new peer\n";
	}
	Start();
}

void TcpServer::StartTimer()
{
	//m_broadcastTimer.cancel();
	m_broadcastTimer.expires_from_now(boost::posix_time::milliseconds(500));
	m_broadcastTimer.async_wait(boost::bind(&TcpServer::HandleTimer, shared_from_this(), boost::asio::placeholders::error ));
}

void TcpServer::HandleTimer(const boost::system::error_code& error)
{
	if (!error)
	{
		BroadcastMessages();
		StartTimer();
	}
}

void TcpServer::BroadcastMessages()
{
	//std::cout << "[Server]broadcasting.";
	m_connections.erase(
		std::remove_if(m_connections.begin(), m_connections.end(),
			[](const std::shared_ptr<TcpConnection>& con) {return con->IsClosed(); }),
		m_connections.end() );

	std::deque<ChatMessage> swappableBuffer;
	{
		std::lock_guard<std::mutex>(m_chatHandler.m_buffersMutex);
		std::swap(swappableBuffer, m_chatHandler.m_outcomingBuffer);
	}
	if( !swappableBuffer.empty() )
	{
        std::cout << "[Server]broadcasting " << swappableBuffer.size() <<"msgs.\n";
	}

	for (auto& connection : m_connections)
	{
        ChatMessage msg;
		connection->SendMessageQueue(swappableBuffer);
    }
}
