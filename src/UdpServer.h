#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "ServerClientFactory.h"
using boost::asio::ip::udp;

class ChatHandler;
class TcpConnection;

class UdpServer: public IServer, public std::enable_shared_from_this<UdpServer>
{
public:
    UdpServer(boost::asio::io_service& io_service, ChatHandler& chathandler);
    virtual void Start();

private:
    void StartReceive();
    void HandleReceive(const boost::system::error_code& error, size_t bytesTransferred);
    void HandleSend(boost::shared_ptr<std::string> message,
                    const boost::system::error_code& error,
                    std::size_t bytes_transferred);


    boost::asio::io_service& m_ioService;
    boost::asio::deadline_timer m_broadcastTimer;
    ChatHandler& m_chatHandler;

    udp::socket             m_socket;
    boost::array<char, 1> m_recv_buffer;

    std::vector<udp::endpoint> m_remoteEndpoints;
};
