#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "TcpClient.h"
#include "ChatHandler.h"

TcpClient::TcpClient(boost::asio::io_service& io_service, ChatHandler& chatHandler)
    :m_ioService(io_service),
    m_timer(m_ioService),
    m_chatHandler( chatHandler )
{
}

void TcpClient::Start()
{
    StartConnection();
    //StartTimer();
}


bool TcpClient::IsConnected()
{
    return m_connection.get();
}

void TcpClient::SendText(const std::string& msgText)
{
    if( IsConnected() )
    {
        m_connection->SendMessage( ChatMessage( m_chatHandler.m_username, msgText ) );
    }
}

std::condition_variable& TcpClient::GetConnectedEvent()
{
    return m_connectedEvent;
}

void TcpClient::StartReconnectTimer()
{
    m_timer.expires_from_now(boost::posix_time::seconds(3));
    m_timer.async_wait(std::bind(&TcpClient::HandleTimer, this));
}

void TcpClient::StartIncomingTimer()
{
    m_timer.expires_from_now(boost::posix_time::seconds(2));
    m_timer.async_wait(std::bind(&TcpClient::HandleTimer, this));
}

void TcpClient::HandleTimer()
{
    if( !IsConnected() )
    {
        StartConnection();
    }
}

void TcpClient::StartConnection()
{
    auto new_connection = std::make_shared<TcpConnection>(m_ioService, m_chatHandler);

    tcp::resolver resolver(m_ioService);
    tcp::resolver::query query("localhost", "7777");
    tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

    boost::asio::async_connect(new_connection->GetSocket(), endpoint_iterator,
        boost::bind(&TcpClient::HandleConnection, this, new_connection,
            boost::asio::placeholders::error, endpoint_iterator));
}

void TcpClient::HandleConnection(std::shared_ptr<TcpConnection> new_connection,
    const boost::system::error_code& error,
    const tcp::resolver::iterator& endpointIter)
{
    if (!error)
    {
        std::cout << "[Client]connected\n";
        m_connection = new_connection;
        m_connection->StartRead();
        SendText("I'm connected.");
        m_connectedEvent.notify_all();

    }
    else
    {
        std::cout << "[Client]connect error. Reconnecting in 3sec\n";
        StartReconnectTimer();
    }

}
