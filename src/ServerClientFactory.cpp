#include "ServerClientFactory.h"

#include "TcpServer.h"
#include "TcpClient.h"
#include "UdpServer.h"
#include "UdpClient.h"
#include "ChatHandler.h"
#include <boost/asio/io_service.hpp>

std::shared_ptr<IServer> ServerClientFactory::CreateServer(const Protocol protocol, boost::asio::io_service& io_service, ChatHandler& chatHandler)
{
	if (protocol == Protocol::k_TCP)
	{
		return std::make_shared<TcpServer>( io_service, chatHandler );
	}
	else
	{
		return  std::make_shared<UdpServer>( io_service, chatHandler );
	}
}

std::shared_ptr<IClient> ServerClientFactory::CreateClient(const Protocol protocol, boost::asio::io_service& io_service, ChatHandler& chatHandler)
{
	if (protocol == Protocol::k_TCP)
	{
		return std::make_shared<TcpClient>(io_service, chatHandler);
	}
	else
	{
		return std::make_shared<UdpClient>(io_service, chatHandler);
	}
}
