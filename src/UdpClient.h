#include <boost/asio.hpp>
#include "TcpConnection.h"
#include "ServerClientFactory.h"
#include <boost/date_time/posix_time/posix_time.hpp>
using boost::asio::ip::udp;

class ChatHandler;

class UdpClient : public IClient, public std::enable_shared_from_this<UdpClient>
{
public:
    UdpClient(boost::asio::io_service& io_service, ChatHandler& chatHandler);
    virtual void Start() override;
    virtual bool IsConnected() override;
    virtual void SendText(const std::string& msgText) override;
    virtual std::condition_variable& GetConnectedEvent() override;

private:

    void StartTimer();

    void StartRead();

    void StartConnection();
    /*void HandleConnection(std::shared_ptr<TcpConnection> new_connection,
        const boost::system::error_code& error,
        const tcp::resolver::results_type::iterator& endpointIter);*/


    boost::asio::io_service& m_ioService;
    ChatHandler& m_chatHandler;
    boost::asio::deadline_timer m_timer;
    udp::socket             m_socket;

    bool                        m_isConnected{ false };
    //std::shared_ptr<TcpConnection>      m_connection;
    std::condition_variable     m_connectedEvent;
};
