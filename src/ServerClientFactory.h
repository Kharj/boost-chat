#pragma once
#include <string>
#include <condition_variable>
#include  <boost/version.hpp>


namespace boost
{
	namespace asio
	{
	#if(BOOST_VERSION / 100 % 1000 >= 66)
		class io_context;
		typedef io_context io_service;
    #else
        class io_service;
    #endif
	};
};
class ChatHandler;

class IServer
{
public:
	virtual void Start() = 0;
};
class IClient
{
public:
	virtual void Start() = 0;
	virtual bool IsConnected() = 0;
	virtual void SendText(const std::string& msgText) = 0;
	virtual std::condition_variable& GetConnectedEvent() = 0;
};

class ServerClientFactory
{
public:
	enum class Protocol : int
	{
		k_TCP = 0,
		k_UDP = 1
	};

	static std::shared_ptr<IServer> CreateServer(const Protocol protocol, boost::asio::io_service& io_service, ChatHandler& chatHandler);
	static std::shared_ptr<IClient> CreateClient(const Protocol protocol, boost::asio::io_service& io_service, ChatHandler& chatHandler);


};
