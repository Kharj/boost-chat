#include <boost/asio.hpp>
#include <string>
#include <condition_variable>
#include "TcpConnection.h"
#include "ServerClientFactory.h"
#include <boost/date_time/posix_time/posix_time.hpp>

using boost::asio::ip::tcp;
class ChatHandler;

class TcpClient : public IClient
{
public:
    TcpClient(boost::asio::io_service& io_service, ChatHandler& chatHandler);
    virtual void Start() override;
    virtual bool IsConnected() override;
    virtual void SendText(const std::string& msgText) override;
    virtual std::condition_variable& GetConnectedEvent() override;
private:

    void StartReconnectTimer();
    void StartIncomingTimer();
    void HandleTimer();
    void StartConnection();
    void HandleConnection(std::shared_ptr<TcpConnection> new_connection,
        const boost::system::error_code& error,
        const tcp::resolver::iterator& endpointIter);


    boost::asio::io_service&    m_ioService;
    boost::asio::deadline_timer m_timer;
    ChatHandler&                m_chatHandler;

    bool                        m_isConnected{false};
    std::shared_ptr<TcpConnection>      m_connection;
    std::condition_variable     m_connectedEvent;
};
