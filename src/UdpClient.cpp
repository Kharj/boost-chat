#include <iostream>
#include <cassert>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "UdpClient.h"
#include "ChatHandler.h"


UdpClient::UdpClient(boost::asio::io_service& io_service, ChatHandler& chatHandler):
    m_ioService(io_service),
    m_chatHandler(chatHandler),
    m_timer(io_service),
    m_socket(io_service)
{
}

void UdpClient::Start()
{
    StartRead();
}

bool UdpClient::IsConnected()
{
    return m_socket.is_open();
}

void UdpClient::SendText(const std::string& msgText)
{
    if (m_socket.is_open())
    {
        auto self = shared_from_this();
        ChatMessage msg(m_chatHandler.m_username, msgText);
        auto bufString = std::make_shared<std::string>(msg.Serialize());
        self->m_socket.async_send(boost::asio::buffer(*bufString),
            [self, bufString](
                const boost::system::error_code& error,
                std::size_t bytes_transferred)
            {
                if (!error)
                {
                    std::cout << "UPD sent " << bytes_transferred << "bytes\n";
                }
                else
                {
                    std::cout << "udp send error: " << error;
                }
            });
    }
}

std::condition_variable& UdpClient::GetConnectedEvent()
{
    return m_connectedEvent;
}

void UdpClient::StartTimer()
{
    m_timer.expires_from_now( boost::posix_time::seconds(5) );
    m_timer.async_wait([](const boost::system::error_code & error)
    {
        if (!error)
        {
            //BroadcastMessages();
            //StartTimer();
        }
    });
}

void UdpClient::StartRead()
{
    udp::resolver resolver(m_ioService);
    udp::resolver::query query(m_chatHandler.m_serverIp,
                                std::to_string( m_chatHandler.m_port ));
    udp::resolver::iterator endpoint_iterator = resolver.resolve(query);

    udp::endpoint endpoint = *endpoint_iterator;
    auto self = shared_from_this();
    //socket.open(udp::v4());

    m_socket.async_connect(*endpoint_iterator, [self, endpoint](const boost::system::error_code& error)
        {
            assert(!error);
            self->m_connectedEvent.notify_all();

            auto bufArray = std::make_shared<boost::array<char, 128>>();
            self->m_socket.async_receive(boost::asio::buffer(*bufArray),
                [self, endpoint, bufArray](
                    const boost::system::error_code& error,
                    std::size_t bytes_transferred)
                {
                    if (!error)
                    {
                        std::cout << "UPD Read " << bytes_transferred << "bytes\n";
                    }
                    else
                    {
                        std::cout << "udp read error: " << error;
                    }
                });
        });

    /*boost::array<char, 1> send_buf  = { 0 };
    socket.send_to(boost::asio::buffer(send_buf), *endpoint_iterator);

    boost::array<char, 128> recv_buf;
    udp::endpoint sender_endpoint;
    size_t len = socket.receive_from(boost::asio::buffer(recv_buf), sender_endpoint);
    */
    //std::string  recvString(recv_buf.data(), len);
    //std::cout<<"[Client]Received "<<recvString<<"\n";

    StartTimer();
}
/*
void UdpClient::HandleConnection(TcpConnection::pointer new_connection,
    const boost::system::error_code& error,
    const tcp::resolver::results_type::iterator& endpointIter)
{
    if (!error)
    {
        std::cout << "[Client]connected\n";
        new_connection->startRead();
    }
    std::cout << "[Client]connect error\n";

}
*/
