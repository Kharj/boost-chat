#include <iostream>
#include <cassert>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "TcpConnection.h"
#include "ChatHandler.h"

const std::string TcpConnection::k_delimeter = "\r\n\r\n";

TcpConnection::TcpConnection(boost::asio::io_service& io_service, ChatHandler& chatHandler)
	: std::enable_shared_from_this<TcpConnection>(),
	m_ioService(io_service),
	m_socket(io_service),
	m_chatHandler(chatHandler)
{
}

tcp::socket& TcpConnection::GetSocket()
{
	return m_socket;
}

bool TcpConnection::IsClosed() const
{
	return m_isClosed;
}


void TcpConnection::StartRead()
{
	//One buffer for all future reads of this peer
	auto incomingStreamBuffer = std::make_shared<boost::asio::streambuf>();
	StartRead(incomingStreamBuffer);
}


void TcpConnection::StartRead(std::shared_ptr<boost::asio::streambuf> streamBuffer)
{
	boost::asio::async_read_until(m_socket, *streamBuffer, k_delimeter,
		boost::bind(&TcpConnection::HandleRead, shared_from_this(),
			streamBuffer,
			boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
}

void TcpConnection::HandleRead( std::shared_ptr<boost::asio::streambuf> streamBuffer,
	const boost::system::error_code& error, const std::size_t bytesTransferred)
{
	if (!error)
	{
		assert(streamBuffer->size() >= bytesTransferred);

		//Parse one message
		std::string incomingText{
		  boost::asio::buffers_begin(streamBuffer->data()),
		  boost::asio::buffers_begin(streamBuffer->data())
		  + bytesTransferred - k_delimeter.size() };

		streamBuffer->consume(bytesTransferred);

		{
			std::lock_guard<std::mutex> guard(m_chatHandler.m_buffersMutex);
			m_chatHandler.m_incomingBuffer.emplace_back(ChatMessage::Deserialize( incomingText ));
		}
		//std::cout << "Successfull read " << bytesTransferred << "bytes\n";
		StartRead(streamBuffer);//Read next
	}
	else if (error == boost::asio::error::connection_reset ||
				error == boost::asio::error::eof)
	{
		std::cout << "Client disconnected." << " \n";
		m_isClosed = true;
	}
	else
	{
		std::cout << "Error reading" << error << " \n";
	}
}

void TcpConnection::StartWrite(const std::string& txt)
{
	//const std::string buffer
	//auto writeBuffer = std::shared_ptr<boost::asio::buffer()
	auto self = shared_from_this();
	auto bufferString = std::make_shared<std::string>(txt);//shared copy
	boost::asio::async_write(m_socket, boost::asio::buffer(*bufferString),
		[bufferString, self](const boost::system::error_code& error, const size_t bytesTransferred)
	{
		if (error == boost::asio::error::connection_reset ||
			error == boost::asio::error::eof)
		{
			std::cout << "Client disconnected." << " \n";
			self->m_isClosed = true;
		}
		else if (error)
		{
			std::cout << "Error writing" << error << " \n";
		}
		//std::cout<<"write ok\n";
	});
}

void TcpConnection::SendMessage(const ChatMessage& msg)
{
    StartWrite( msg.Serialize() + TcpConnection::k_delimeter );
}

void TcpConnection::SendMessageQueue(const std::deque<ChatMessage>& msgs)
{
    if( !msgs.empty() )
    {
        std::string batched;
        for (auto& msg : msgs)
        {
            batched += msg.Serialize() + TcpConnection::k_delimeter;
        }
        StartWrite( batched );
    }
}
