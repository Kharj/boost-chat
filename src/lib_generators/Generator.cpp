#include "Generator.h"
#include <sstream>

std::string Generator::Generate()
{
    std::stringstream ss;
    ss << "Gen" << m_counter;
    m_counter++;

    return ss.str();
}