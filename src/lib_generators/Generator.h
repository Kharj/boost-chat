#include <string>

class Generator
{
public:
    std::string Generate();
private:
    int m_counter{ 0 };
};