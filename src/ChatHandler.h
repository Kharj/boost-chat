#if !defined(CHAT_HANDLER_H)
#define CHAT_HANDLER_H

#include <deque>
#include <string>
#include <mutex>
#include <cassert>

class ChatMessage
{
public:
    ChatMessage()
    {}

    ChatMessage( const std::string& username, const std::string& text ):
        m_username( username ),
        m_text( text )
    {};

    static ChatMessage Deserialize(const std::string& serialized)
    {
        ChatMessage res;
        if( !serialized.empty() )
        {
            unsigned char usernameLen = static_cast<unsigned char>( serialized[0] );
            assert( serialized.length() >= (unsigned int)usernameLen + 1 );
            res.m_username = serialized.substr( 1, usernameLen );
            res.m_text = serialized.substr(  1 + usernameLen, serialized.length() - 1 - usernameLen  );
        }
        return res;
    }

    const std::string Serialize() const
    {
        unsigned char usernameLen = m_username.length();
        std::string res;
        res += (char)usernameLen;
        res += m_username;
        res += m_text;
        return res;
    }

    std::string m_username;
    std::string m_text;
};

class ChatHandler
{
public:
    //void    PushMessage(ChatMessage&& msg);

    std::deque<ChatMessage>     m_incomingBuffer;
    std::deque<ChatMessage>     m_outcomingBuffer;
    std::mutex                  m_buffersMutex;


    std::string     m_serverIp;
    int             m_port;
    std::string     m_username;

};

#endif
